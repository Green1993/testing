import java.util.Scanner;

/**
 * Типы данных в Java (примитивные и ссылочный типы)
 * Объявление переменных
 * Функции (их объявление и вызов)
 * Ввод/вывод данных
 * Операторы и циклы (if else, for, while)
 * <p>
 * Задача1: Найти ошибку в методе isPrimeNumber, который проверяет, что число простое
 * Задача2: Написать метод isDivByTwo, который проверяет, что число делится на 2 и на 3
 */

public class Main {

    // Входная точка в программе, с которой все начинается
    public static void main(String[] args) {
        System.out.println("Введите число n: ");

        // Класс Scanner нужен для считывания данных из консоли
        Scanner scanner = new Scanner(System.in);

        while (scanner.hasNextInt()) { // В цикле ожидаем ввода целого числа, в противном случае программа завершится

            // Вызов метода nextInt() ожидает ввода из консоли данных типа int (нужно ввести число и нажать Enter)
            int n = scanner.nextInt();

            if (isPrimeNumber(n)) {
                System.out.println("PRIME");
            } else {
                System.out.println("NOT PRIME");
            }

            if (isDivByTwoAndThree(n)) {
                System.out.println("DIV BY 2 AND 3");
            } else {
                System.out.println("NOT DIV BY 2 AND 3");
            }
        }
    }//

    private static boolean isPrimeNumber(int n) {
        boolean isPrime = true;

        for (int i = 1; i <= n; i++) {

            if (i == 1) {
                continue;
            }

            if (i == n) {
                continue;
            }

            if (n % i == 0) {
                isPrime = false;
                break;
            } else  isPrime = true;
        }

        return isPrime;
    }

    private static boolean isDivByTwoAndThree(int n) {
        boolean isDivByTwoAndThree = true;
        if (n % 2 == 0 && n % 3 == 0){
                return true;
            } else{isDivByTwoAndThree = false;}

        return isDivByTwoAndThree;
        }

}

